exports.carData = [{
  "type": "park",
  "status": true,
  "number": "KA-01-HH-1234"
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-HH-9999"
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-BB-0001"
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-HH-7777"
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-HH-2701"
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-HH-3141"
},
{
  "type": "leave",
  "number": "KA-01-HH-3141",
  "hour": 4
}, {
  "type": "park",
  "status": true,
  "number": "KA-01-P-333"
}, {
  "type": "park",
  "status": true,
  "number": "DL-12-AA-9999"
}, {
  "type": "leave",
  "number": "KA-01-HH-1234",
  "hour": 4
}, {
  "type": "leave",
  "number": "KA-01-BB-0001",
  "hour": 6
}, {
  "type": "leave",
  "number": "DL-12-AA-9999",
  "hour": 2
}, {
  "type": "park",
  "status": true,
  "number": "KA-09-HH-0987"
}, {
  "type": "park",
  "status": true,
  "number": "CA-09-IO-1111"
}, {
  "type": "park",
  "status": true,
  "number": "KA-09-HH-0123"
}
]

