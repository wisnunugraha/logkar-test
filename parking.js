const e = require('express');
const {
  carData
} = require('./car');

// Class of Parking
class ParkingNew {

  /**
   * Describe Define Variable 
   * 1. slotsParking  = Empty Array
   * 2. firstHour     = Compile at one time it's 2 Hours
   * 3. firstFeeHour  = Charge applicable is 10 Dollars for firstHour
   * 4. feePerHour    = After firstHour, $10 for every additional hour
   * 4. onceShow      = is to show boolean first tim remove data and show data
   */

  slotsParking = [];
  freeFirstHour = 2;
  firstFeeHour = 10;
  feePerHour = 10;

  /**
   * 
   * @param {*} carLength
   * -- it is size of car to going to parking 
   */
  constructor(carLength) {
    this.carLength = carLength;
  }

  /**
   * Describe dataParking fucntion :
   * we going to :
   * 1. generateSlotIdParking to set our maximum slot parking;
   * 2. checkSlotParking checking our data is "park" or "leave"
   */
  dataParking() {
    console.log('create_parking_lot', this.carLength +' slots')
    this.generateSlotIdParking();
    this.checkSlotParking();
    console.log(this.slotsParking)
  }

  /**
   * Describe generateSlotIdParking fucntion :
   * we going to :
   * 1. generateSlotIdParking to generate array slotsParking
   *    a. id is index data or uniqe data
   *    b. no is number loop from 1 
   *    c. number is number uniqe car
   *    d. status is bolaean such as park (true) or leave (false)
   */
  generateSlotIdParking() {
    let no = 1;
    console
    for (let i = 0; i < this.carLength; i++) {
      this.slotsParking.push({
        id: i,
        no: no++,
        number: null,
        status: false
      });
    }
  }

  /**
   * Describe checkSlotParking fucntion :
   * we going to :
   * 1. checkSlotParking checking our data is "park" or "leave"
   * 2. saveSlotParking if carData when car type 'park'
   * 3. removelotParking if carData when car type 'leave'
   */
  checkSlotParking() {
    for (let i = 0; i < carData.length; i++) {
      if (carData[i].type === 'park') {
        this.saveSlotParking(carData[i])
      };
      if (carData[i].type === 'leave') {
        this.removelotParking(carData[i])
      };
    }
  }

  /**
   * 
   * @param {*} cars
   * 
   * Variable id is index
   * Variable no is number 
   * 
   * for is loop to find slotsParking when status it 'false'
   * if i found i will put id:id and no:no 
   * after that we find index obj by Variable id
   * and put/update data to slotsParking 
   * 
   */
  saveSlotParking(cars) {
    let id;
    let no;
    for (let i = 0; i < this.slotsParking.length; i++) {
      if (this.slotsParking[i].status == false) {
        id = this.slotsParking[i].id;
        no = this.slotsParking[i].no;
        break;
      };
    }
    if (id == undefined) {
      console.log('Sorry, parking lot is full')
    }else{
      console.log('Allocated slot number: '+ no)
      let objIndex = this.slotsParking.findIndex((x) => x.id == id);
      if (objIndex >= 0) {
        this.slotsParking[objIndex].number = cars.number;
        this.slotsParking[objIndex].status = true;
      }

    }
  }


  /**
   * 
   * @param {*} cars
   * 
   * Variable id is index
   * Variable no is number 
   * 
   * for is loop to find slotsParking when number uniqe cars same as data we want delete
   * if i found i will put id:id and no:no 
   * if no undefined it will show 'Not Found'
   * if no exist we can replace number to be null and status to be 'false'
   * 
   */
  removelotParking(cars) {
    let id;
    let no;
    
    for (let i = 0; i < this.slotsParking.length; i++) {
      if (this.slotsParking[i].number == cars.number) {
        id = this.slotsParking[i].id;
        no = this.slotsParking[i].no;
        break;
      };
    }

    if (no == undefined) {
      console.log('Registration number ' + cars.number + ' not found');
    } else {
      let perHour = (cars.hour - this.freeFirstHour)*this.feePerHour;
      let firstFee = (perHour+this.firstFeeHour);
      console.log('Registration number ' + cars.number + ' with Slot Number ' + no + ' is free with Charge '+ firstFee);
      let objIndex = this.slotsParking.findIndex((x) => x.id == id);
      if (objIndex >= 0) {
        this.slotsParking[objIndex].number = null;
        this.slotsParking[objIndex].status = false;
      }

    }
  }

}


let test = new ParkingNew(6);
test.dataParking()
process.exit()