# logkar-test

this test about Parking Lot

## Getting started

 language :
- Node.js v16.13.2.

```
git clone https://gitlab.com/wisnunugraha/logkar-test.git
cd logkar-test
node parking.js
```

## Result Of Parking Lot
```
create_parking_lot 6 slots
Allocated slot number: 1
Allocated slot number: 2
Allocated slot number: 3
Allocated slot number: 4
Allocated slot number: 5
Allocated slot number: 6
Registration number KA-01-HH-3141 with Slot Number 6 is free with Charge 30
Allocated slot number: 6
Sorry, parking lot is full
Registration number KA-01-HH-1234 with Slot Number 1 is free with Charge 30
Registration number KA-01-BB-0001 with Slot Number 3 is free with Charge 50
Registration number DL-12-AA-9999 not found
Allocated slot number: 1
Allocated slot number: 3
Sorry, parking lot is full
[
  { id: 0, no: 1, number: 'KA-09-HH-0987', status: true },
  { id: 1, no: 2, number: 'KA-01-HH-9999', status: true },
  { id: 2, no: 3, number: 'CA-09-IO-1111', status: true },
  { id: 3, no: 4, number: 'KA-01-HH-7777', status: true },
  { id: 4, no: 5, number: 'KA-01-HH-2701', status: true },
  { id: 5, no: 6, number: 'KA-01-P-333', status: true }
]
```

